# Game 2048
[Wiki](https://ru.wikipedia.org/wiki/2048_(%D0%B8%D0%B3%D1%80%D0%B0))
[Release](https://bitbucket.org/michmv/e2048/src/release)

[Demo](http://mmv-module.herokuapp.com/demos/e2048/)

The application is written in [elm language](https://elm-lang.org/) and then compiled in javascript.

## Build
Before need install:

* elm
* lessc
* uglifyjs
* csso

```
$ git clone https://bitbucket.org/michmv/e2048.git
$ cd e2048
$ make min
```

## Use

Open in browser `index.html`.
