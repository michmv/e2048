module G2048Test exposing (..)

import Expect exposing (Expectation)
import Fuzz exposing (Fuzzer, int, list, string)
import G2048 as G
import Test exposing (..)


type alias ChipTest =
    { point : G.Point
    , old_point : G.Point
    , value : Int
    , delete : Bool
    , new : Bool
    }


classChipInstance : G.ClassChip ChipTest
classChipInstance =
    G.ClassChip
        (\x -> x.point)
        (\x p -> { x | point = p })
        (\x p -> { x | old_point = p })
        (\x -> x.value)
        (\x v -> { x | value = v })
        (\x -> x.delete)
        (\x d -> { x | delete = d })
        (\x n -> { x | new = n })


defaultChip =
    ChipTest ( 0, 0 ) ( 0, 0 ) 0 False False


suite : Test
suite =
    describe "G2048"
        [ describe "getListEmptyPoint"
            [ test "Empty" <|
                \_ ->
                    G.getListEmptyPoint classChipInstance []
                        |> Expect.equal [ ( 0, 0 ), ( 1, 0 ), ( 2, 0 ), ( 3, 0 ), ( 0, 1 ), ( 1, 1 ), ( 2, 1 ), ( 3, 1 ), ( 0, 2 ), ( 1, 2 ), ( 2, 2 ), ( 3, 2 ), ( 0, 3 ), ( 1, 3 ), ( 2, 3 ), ( 3, 3 ) ]
            , test "One" <|
                \_ ->
                    G.getListEmptyPoint classChipInstance [ defaultChip ]
                        |> Expect.equal [ ( 1, 0 ), ( 2, 0 ), ( 3, 0 ), ( 0, 1 ), ( 1, 1 ), ( 2, 1 ), ( 3, 1 ), ( 0, 2 ), ( 1, 2 ), ( 2, 2 ), ( 3, 2 ), ( 0, 3 ), ( 1, 3 ), ( 2, 3 ), ( 3, 3 ) ]
            ]
        , describe "move"
            [ test "0002 -> 2000" <|
                \_ ->
                    G.move classChipInstance
                        [ ChipTest ( 3, 0 ) ( 0, 0 ) 2 False False
                        ]
                        defaultChip
                        G.Left
                        |> Expect.equal
                            [ ChipTest ( 0, 0 ) ( 3, 0 ) 2 False False
                            ]
            , test "0022 -> 4000" <|
                \_ ->
                    G.move classChipInstance
                        [ ChipTest ( 2, 0 ) ( 0, 0 ) 2 False False
                        , ChipTest ( 3, 0 ) ( 0, 0 ) 2 False False
                        ]
                        defaultChip
                        G.Left
                        |> Expect.equal
                            [ ChipTest ( 0, 0 ) ( 3, 0 ) 2 True False
                            , ChipTest ( 0, 0 ) ( 2, 0 ) 2 True False
                            , ChipTest ( 0, 0 ) ( 0, 0 ) 4 False True
                            ]
            , test "0222 -> 4200" <|
                \_ ->
                    G.move classChipInstance
                        [ ChipTest ( 1, 0 ) ( 0, 0 ) 2 False False
                        , ChipTest ( 2, 0 ) ( 0, 0 ) 2 False False
                        , ChipTest ( 3, 0 ) ( 0, 0 ) 2 False False
                        ]
                        defaultChip
                        G.Left
                        |> Expect.equal
                            [ ChipTest ( 0, 0 ) ( 2, 0 ) 2 True False
                            , ChipTest ( 0, 0 ) ( 1, 0 ) 2 True False
                            , ChipTest ( 0, 0 ) ( 0, 0 ) 4 False True
                            , ChipTest ( 1, 0 ) ( 3, 0 ) 2 False False
                            ]
            , test "2222 -> 4400" <|
                \_ ->
                    G.move classChipInstance
                        [ ChipTest ( 0, 0 ) ( 0, 0 ) 2 False False
                        , ChipTest ( 1, 0 ) ( 0, 0 ) 2 False False
                        , ChipTest ( 2, 0 ) ( 0, 0 ) 2 False False
                        , ChipTest ( 3, 0 ) ( 0, 0 ) 2 False False
                        ]
                        defaultChip
                        G.Left
                        |> Expect.equal
                            [ ChipTest ( 0, 0 ) ( 1, 0 ) 2 True False
                            , ChipTest ( 0, 0 ) ( 0, 0 ) 2 True False
                            , ChipTest ( 0, 0 ) ( 0, 0 ) 4 False True
                            , ChipTest ( 1, 0 ) ( 3, 0 ) 2 True False
                            , ChipTest ( 1, 0 ) ( 2, 0 ) 2 True False
                            , ChipTest ( 1, 0 ) ( 1, 0 ) 4 False True
                            ]
            ]
        ]
