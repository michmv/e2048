all: bin bin/elm.js bin/style.css

bin/elm.js: src/Main.elm src/MList.elm src/MKeyboard.elm src/G2048.elm
	elm make src/Main.elm --output bin/elm.js

bin/style.css: src/style.less
	lessc src/style.less > bin/style.css

bin:
	mkdir -p bin	

min: all
	elm make src/Main.elm --output --optimize bin/elm.js
	uglifyjs bin/elm.js --compress 'pure_funcs="F2,F3,F4,F5,F6,F7,F8,F9,A2,A3,A4,A5,A6,A7,A8,A9",pure_getters,keep_fargs=false,unsafe_comps,unsafe' | uglifyjs --mangle --output=bin/elm.js
	csso bin/style.css -o bin/style.css

clean:
	rm -R bin
