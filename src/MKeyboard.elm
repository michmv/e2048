module MKeyboard exposing
    ( Binds, Handler(..), checkKey, checkKeyAll, checkKeyAny
    , init, keyDownEvent, keyUpEvent, onKeyDown, onKeyUp
    , ClassAction
    )

{-| MKeyboard
@version 1.1.1

@docs Binds, Handler, checkKey, checkKeyAll, checkKeyAny

@docs init, keyDownEvent, keyUpEvent, onKeyDown, onKeyUp

@docs ClassAction, KeyDown, KeyUp

-}

import Browser.Events as E
import Json.Decode as D


type Handler model msg
    = KeyDown (String -> List String -> Bool) (model -> ( model, Cmd msg ))
    | KeyUp (String -> List String -> Bool) (model -> ( model, Cmd msg ))


type alias Binds model msg =
    ( List String, List (Handler model msg) )


type alias ClassAction model msg =
    { get : model -> Binds model msg
    , set : model -> Binds model msg -> model
    }


{-| Check single button
If second parameter True then one event for this button
-}
checkKey : String -> Bool -> (String -> List String -> Bool)
checkKey id one =
    \key list ->
        if one then
            key == id && not (List.any ((==) key) list)

        else
            key == id


{-| Check have all botton
If second parameter True then one event for this button
-}
checkKeyAll : List String -> Bool -> (String -> List String -> Bool)
checkKeyAll ids one =
    \key list ->
        if one then
            List.any ((==) key) ids
                && checkListInList (==) ids (key :: list)
                && not (checkListInList (==) ids list)

        else
            List.any ((==) key) ids
                && checkListInList (==) ids (key :: list)


{-| Check have any botton
If second parameter True then one event for this button
-}
checkKeyAny : List String -> Bool -> (String -> List String -> Bool)
checkKeyAny ids one =
    \key list ->
        if one then
            List.any ((==) key) ids
                && not (List.any ((==) key) list)

        else
            List.any ((==) key) ids


init : List (Handler model msg) -> Binds model msg
init handlers =
    ( [], handlers )


keyDownEvent =
    keyAction addKey onlyDown


keyUpEvent =
    keyAction removeKey onlyUp


onKeyDown : (String -> msg) -> Sub msg
onKeyDown msg =
    E.onKeyDown (D.map msg keyDecoder)


onKeyUp : (String -> msg) -> Sub msg
onKeyUp msg =
    E.onKeyUp (D.map msg keyDecoder)


keyAction :
    (List String -> String -> List String)
    -> (Handler model msg -> Bool)
    -> ClassAction model msg
    -> String
    -> model
    -> ( model, Cmd msg )
keyAction keys_action filter_type class key model =
    let
        ( keys, handlers ) =
            class.get model

        ( model_new, cmd ) =
            callHandlers key keys model (List.filter filter_type handlers)
    in
    ( class.set model_new ( keys_action keys key, handlers ), cmd )


onlyDown : Handler model msg -> Bool
onlyDown handler =
    case handler of
        KeyDown _ _ ->
            True

        _ ->
            False


onlyUp : Handler model msg -> Bool
onlyUp key =
    case key of
        KeyUp _ _ ->
            True

        _ ->
            True


callHandlers :
    String
    -> List String
    -> model
    -> List (Handler model msg)
    -> ( model, Cmd msg )
callHandlers key keys model handlers =
    case handlers of
        [] ->
            ( model, Cmd.none )

        x :: xs ->
            case callHandler key keys model x of
                Just r ->
                    r

                Nothing ->
                    callHandlers key keys model xs


callHandler :
    String
    -> List String
    -> model
    -> Handler model msg
    -> Maybe ( model, Cmd msg )
callHandler key keys model handler =
    case handler of
        KeyDown check func ->
            if check key keys then
                Just (func model)

            else
                Nothing

        KeyUp check func ->
            if check key keys then
                Just (func model)

            else
                Nothing


keyDecoder : D.Decoder String
keyDecoder =
    D.field "key" D.string


addKey : List String -> String -> List String
addKey list key =
    if findKey list key then
        list

    else
        key :: list


removeKey : List String -> String -> List String
removeKey list key =
    List.filter (\n -> n /= key) list


findKey : List String -> String -> Bool
findKey list key =
    case list of
        [] ->
            False

        x :: xs ->
            if x == key then
                True

            else
                findKey xs key


checkListInList : (a -> b -> Bool) -> List a -> List b -> Bool
checkListInList func its list =
    let
        listBool =
            List.map (\x -> List.any (func x) list) its
    in
    if List.length its == 0 then
        False

    else
        List.foldl (&&) True listBool
