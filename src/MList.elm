module MList exposing
    ( findIt, checkListInList
    , byIndexFromList, transpose
    )

{-| MList - more function for `List`
@version 1.1.0

@docs findIt, checkListInList, transponse, byindexFromList

-}

import Array


{-| Find element in list
-}
findIt : (a -> Bool) -> List a -> Maybe a
findIt func list =
    case list of
        [] ->
            Nothing

        x :: xs ->
            if func x then
                Just x

            else
                findIt func xs


checkListInList : (a -> b -> Bool) -> List a -> List b -> Bool
checkListInList func its list =
    let
        listBool =
            List.map (\x -> List.any (func x) list) its
    in
    if List.length its == 0 then
        False

    else
        List.foldl (&&) True listBool


{-|

    transpose [ [ 1, 2, 3, 4 ], [ 1, 2, 3, 4 ] ] --> [[1,1],[2,2],[3,3],[4,4]]

    transpose [ [ 10, 11 ], [ 20 ], [], [ 30, 31, 32 ] ] --> [[10,20,30],[11,31],[32]]

-}
transpose : List (List a) -> List (List a)
transpose ll =
    let
        heads =
            List.map (List.take 1) ll |> List.concat

        tails =
            List.map (List.drop 1) ll
    in
    if List.isEmpty heads then
        []

    else
        heads :: transpose tails


byIndexFromList : a -> List a -> Int -> a
byIndexFromList default list index =
    let
        value =
            Array.get index (Array.fromList list)
    in
    case value of
        Just r ->
            r

        Nothing ->
            default
