module G2048 exposing (..)

import Array
import MList


type Field chip
    = One chip
    | Union (List chip)
    | Empty


type alias Game chip =
    List (List (Field chip))


type alias Point =
    ( Int, Int )


type alias ClassChip a =
    { getPoint : a -> Point
    , setPoint : a -> Point -> a
    , setOldPoint : a -> Point -> a
    , getValue : a -> Int
    , setValue : a -> Int -> a
    , getDelete : a -> Bool
    , setDelete : a -> Bool -> a
    , setNew : a -> Bool -> a
    }


type Move
    = Left
    | Right
    | Up
    | Down


type Rotate
    = ToLeft
    | ToRight


defaultPoint =
    ( 0, 0 )


defaultGame : Game chip
defaultGame =
    [ [ Empty, Empty, Empty, Empty ]
    , [ Empty, Empty, Empty, Empty ]
    , [ Empty, Empty, Empty, Empty ]
    , [ Empty, Empty, Empty, Empty ]
    ]


getListEmptyPoint : ClassChip chip -> List chip -> List Point
getListEmptyPoint class list =
    let
        loop1 list_ x y =
            case list_ of
                [] ->
                    []

                l :: ls ->
                    loop2 l x y ++ loop1 ls x (y + 1)

        loop2 list_ x y =
            case list_ of
                [] ->
                    []

                l :: ls ->
                    case l of
                        Empty ->
                            [ ( x, y ) ] ++ loop2 ls (x + 1) y

                        _ ->
                            loop2 ls (x + 1) y
    in
    loop1 (chipsToGame class list) 0 0


createChip : ClassChip chip -> chip -> Point -> Int -> chip
createChip class default point value =
    let
        cost =
            if value == 9 then
                4

            else
                2
    in
    class.setOldPoint
        (class.setDelete
            (class.setPoint
                (class.setValue default cost)
                point
            )
            False
        )
        point


move : ClassChip chip -> List chip -> chip -> Move -> List chip
move class chips new type_move =
    let
        game =
            chipsToGame class (prepare chips)

        prepare list =
            List.map
                (\y ->
                    class.setOldPoint
                        (class.setNew y False)
                        (class.getPoint y)
                )
                (List.filter (\x -> not <| class.getDelete x) list)
    in
    gameToChips class new <|
        case type_move of
            Left ->
                moveLeftGame class game

            Right ->
                rotate ToRight <|
                    rotate ToRight <|
                        moveLeftGame class <|
                            rotate ToLeft <|
                                rotate ToLeft game

            Up ->
                rotate ToRight <| moveLeftGame class <| rotate ToLeft game

            Down ->
                rotate ToLeft <| moveLeftGame class <| rotate ToRight game


equalPoint : Point -> Point -> Bool
equalPoint ( x1, y1 ) ( x2, y2 ) =
    x1 == x2 && y1 == y2



-----------------------------------------------------------------------------


gameToChips : ClassChip chip -> chip -> Game chip -> List chip
gameToChips class new game =
    let
        row : Int -> List (List (Field chip)) -> List chip
        row y list =
            case list of
                [] ->
                    []

                l :: ls ->
                    col 0 y l ++ row (y + 1) ls

        col : Int -> Int -> List (Field chip) -> List chip
        col x y list =
            case list of
                [] ->
                    []

                l :: ls ->
                    fieldToChip x y l ++ col (x + 1) y ls

        fieldToChip : Int -> Int -> Field chip -> List chip
        fieldToChip x y field =
            case field of
                One i ->
                    [ class.setPoint i ( x, y ) ]

                Union l ->
                    List.map
                        (\m ->
                            class.setDelete (class.setPoint m ( x, y )) True
                        )
                        l
                        ++ [ class.setOldPoint
                                (class.setPoint (new_ l) ( x, y ))
                                ( x, y )
                           ]

                Empty ->
                    []

        new_ l =
            let
                value =
                    class.getValue <| Maybe.withDefault new <| List.head l
            in
            class.setValue
                (class.setNew new True)
                (value * 2)
    in
    row 0 game


moveLeftGame : ClassChip chip -> Game chip -> Game chip
moveLeftGame class game =
    List.map (\l -> moveLeftFill <| moveLeftUnion class <| moveLeftLine l) game


moveLeftFill : List (Field chip) -> List (Field chip)
moveLeftFill list =
    List.take 4 <| list ++ [ Empty, Empty, Empty, Empty ]


moveLeftUnion : ClassChip chip -> List (Field chip) -> List (Field chip)
moveLeftUnion class list =
    case list of
        [] ->
            []

        x :: [] ->
            [ x ]

        x :: xs ->
            List.foldl (unionField class) [ x ] xs


unionField : ClassChip chip -> Field chip -> List (Field chip) -> List (Field chip)
unionField class field list =
    let
        re =
            List.reverse list

        ( body, last ) =
            ( List.reverse <| Maybe.withDefault [] (List.tail re)
            , Maybe.withDefault Empty (List.head re)
            )
    in
    case ( last, field ) of
        ( One l, One f ) ->
            if class.getValue l == class.getValue f then
                body ++ [ Union [ f, l ] ]

            else
                list ++ [ field ]

        _ ->
            list ++ [ field ]


moveLeftLine : List (Field chip) -> List (Field chip)
moveLeftLine list =
    List.filter
        (\x ->
            case x of
                One _ ->
                    True

                _ ->
                    False
        )
        list


chipsToGame : ClassChip chip -> List chip -> Game chip
chipsToGame class list =
    List.foldl (setChipInGame class) defaultGame list


setChipInGame : ClassChip chip -> chip -> Game chip -> Game chip
setChipInGame class chip game =
    let
        ( x, y ) =
            class.getPoint chip

        array_game =
            Array.fromList game

        line_array =
            Array.fromList (Maybe.withDefault [] (Array.get y array_game))

        new_line =
            Array.set x (One chip) line_array
    in
    Array.toList <| Array.set y (Array.toList new_line) array_game


rotate : Rotate -> Game chip -> Game chip
rotate action game =
    case action of
        ToRight ->
            MList.transpose <| List.reverse game

        ToLeft ->
            List.reverse <| MList.transpose game
