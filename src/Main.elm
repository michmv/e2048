module Main exposing (..)

import Animation as A
import Array
import Browser
import Browser.Dom exposing (getViewport)
import Browser.Events
    exposing
        ( onAnimationFrameDelta
        , onKeyDown
        , onKeyPress
        , onKeyUp
        , onResize
        )
import G2048 as G
import Html exposing (..)
import Html.Attributes exposing (class, id, style)
import Html.Events exposing (onClick)
import Html.Keyed as Keyed
import Html.Lazy exposing (lazy)
import MKeyboard as K
import MList
import Random
import String as S
import Task


main =
    Browser.element
        { init = init
        , update = update
        , view = view
        , subscriptions = subscriptions
        }


type alias Model =
    { n : Int
    , width : Float
    , height : Float
    , need_chip : Int -- how many chips do need to create
    , chips : List Chip
    , size : Size
    , clock : A.Clock
    , binds : BindsBox
    , score : Int
    }


type BindsBox
    = BindsBox (K.Binds Model Msg)


type Msg
    = Init Int Int
    | NewChip (List G.Point) ( Int, Int )
    | Resize Int Int
    | KeyUp String
    | KeyDown String
    | Tick Float
    | NewGame


type alias Size =
    { app : Float -- size application
    , game : Float -- size game
    , padding_game : Float
    , box : Float -- size box for chip
    , padding_chip : Float
    , chip : Float -- size chip
    , text : Float -- height text
    , padding_text : Float
    , score : Float -- height score
    , padding_score : Float
    }


type alias Chip =
    { id : Int
    , value : Int
    , x_a : A.Animation
    , y_a : A.Animation
    , size_a : A.Animation
    , point : G.Point
    , old_point : G.Point
    , delete : Bool
    , new : Bool
    }


classChipInstance : G.ClassChip Chip
classChipInstance =
    G.ClassChip
        (\x -> x.point)
        (\x p -> { x | point = p })
        (\x p -> { x | old_point = p })
        (\x -> x.value)
        (\x v -> { x | value = v })
        (\x -> x.delete)
        (\x d -> { x | delete = d })
        (\x n -> { x | new = n })


classActionInstance : K.ClassAction Model Msg
classActionInstance =
    { get = \x -> (\(BindsBox m) -> m) x.binds
    , set = \x n -> { x | binds = BindsBox n }
    }


defaultSize =
    Size 0 0 0 0 0 0 0 0 0 0


defaultChip =
    Chip
        0
        0
        (A.static 0)
        (A.static 0)
        (A.static 0)
        G.defaultPoint
        G.defaultPoint
        False
        False


init : () -> ( Model, Cmd Msg )
init _ =
    ( Model 1
        0
        0
        2
        []
        defaultSize
        0
        binds
        0
    , Task.perform
        (\{ viewport } ->
            Init (round viewport.width) (round viewport.height)
        )
        getViewport
    )


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        Init width height ->
            ( resize model width height
            , randomChipCmd model
            )

        NewChip list ( index, value ) ->
            newChip model list index value

        Resize width height ->
            ( resize model width height
            , Cmd.none
            )

        KeyUp key ->
            K.keyUpEvent classActionInstance key model

        KeyDown key ->
            K.keyDownEvent classActionInstance key model

        Tick t ->
            ( { model | clock = model.clock + t }, Cmd.none )

        NewGame ->
            newGame model


view : Model -> Html Msg
view model =
    let
        size =
            model.size

        app_left =
            if model.width > model.height then
                model.width / 2 - size.app / 2

            else
                0

        app_top =
            if model.height > model.width then
                model.height / 2 - size.app / 2

            else
                0

        button_padding =
            size.padding_score / 2
    in
    div
        [ id "app"
        , style "width" <| S.fromFloat size.app ++ "px"
        , style "height" <| S.fromFloat size.app ++ "px"
        , style "left" <| S.fromFloat app_left ++ "px"
        , style "top" <| S.fromFloat app_top ++ "px"
        ]
        [ div
            [ id "game"
            , style "width" <| S.fromFloat size.game ++ "px"
            , style "height" <| S.fromFloat size.game ++ "px"
            , style "left" <| S.fromFloat size.padding_game ++ "px"
            , style "top" <| S.fromFloat size.padding_game ++ "px"
            ]
            [ div
                [ id "score"
                , style "top" <| S.fromFloat (size.score * -1 - size.padding_score) ++ "px"
                , style "font-size" <| S.fromFloat size.score ++ "px"
                , style "line-height" <| S.fromFloat size.score ++ "px"
                ]
                [ text <| "Score: " ++ S.fromInt model.score ]
            , div
                [ id "new_game"
                , style "top" <|
                    S.fromFloat
                        (size.score * -1 - size.padding_score - button_padding)
                        ++ "px"
                , style "font-size" <| S.fromFloat size.score ++ "px"
                , style "line-height" <| S.fromFloat size.score ++ "px"
                , style "padding-top" <| S.fromFloat button_padding ++ "px"
                , style "padding-bottom" <| S.fromFloat button_padding ++ "px"
                , onClick NewGame
                ]
                [ text "New game" ]
            , Keyed.node "div" [] (List.map (viewKeyedChip model) model.chips)
            ]
        , div
            [ id "text"
            , style "width" <| S.fromFloat size.app ++ "px"
            , style "top" <| S.fromFloat size.padding_text ++ "px"
            , style "font-size" <| S.fromFloat size.text ++ "px"
            , style "line-height" <| S.fromFloat size.text ++ "px"
            ]
            [ text "←, ↑, →, ↓ for move, \"N\" for new game" ]
        ]


viewKeyedChip : Model -> Chip -> ( String, Html Msg )
viewKeyedChip model chip =
    ( String.fromInt chip.id, lazy (viewChip model) chip )


viewChip : Model -> Chip -> Html Msg
viewChip model chip =
    let
        left =
            A.animate model.clock chip.x_a

        top =
            A.animate model.clock chip.y_a

        chip_size =
            A.animate model.clock chip.size_a

        chip_text_size =
            chip_size * font_size / 100

        font_size =
            if chip.value > 99999 then
                20

            else if chip.value > 9999 then
                25

            else
                30

        offset =
            model.size.box / 2 - chip_size / 2
    in
    div
        [ class <| "chip v" ++ S.fromInt chip.value
        , style "width" <| S.fromFloat chip_size ++ "px"
        , style "height" <| S.fromFloat chip_size ++ "px"
        , style "left" <| S.fromFloat (left + offset) ++ "px"
        , style "top" <| S.fromFloat (top + offset) ++ "px"
        ]
        [ div
            [ class "value"
            , style "font-size" <| S.fromFloat chip_text_size ++ "px"
            , style "line-height" <| S.fromFloat chip_size ++ "px"
            ]
            [ text <| S.fromInt chip.value ]
        ]


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ onResize Resize
        , K.onKeyDown KeyDown
        , K.onKeyUp KeyUp
        , onAnimationFrameDelta Tick
        ]


{-| All binds for this application
-}
binds : BindsBox
binds =
    BindsBox
        (K.init
            [ K.KeyDown (K.checkKeyAny [ "n", "N", "т", "Т" ] True) newGame
            , K.KeyDown (K.checkKey "ArrowLeft" True) (nextStep G.Left)
            , K.KeyDown (K.checkKey "ArrowUp" True) (nextStep G.Up)
            , K.KeyDown (K.checkKey "ArrowDown" True) (nextStep G.Down)
            , K.KeyDown (K.checkKey "ArrowRight" True) (nextStep G.Right)
            ]
        )


newGame : Model -> ( Model, Cmd Msg )
newGame model =
    let
        new_model =
            { model | chips = [], need_chip = 2, score = 0 }
    in
    ( new_model, randomChipCmd new_model )


nextStep : G.Move -> Model -> ( Model, Cmd Msg )
nextStep move model =
    let
        next =
            G.move classChipInstance model.chips defaultChip move

        update_ : Int -> List Chip -> List Chip -> ( Int, List Chip )
        update_ id list res =
            case list of
                [] ->
                    ( id, res )

                x :: xs ->
                    if x.new then
                        update_ (id + 1) xs (updateXYSIZE { x | id = id } :: res)

                    else
                        update_ id xs (updateXYSIZE x :: res)

        updateXYSIZE x =
            updateSize <| updateMoveX <| updateMoveY x

        updateSize x =
            if x.new then
                { x | size_a = animateCreateChip model }

            else if x.delete then
                { x | size_a = animateDeleteChip model }

            else
                { x | size_a = A.static model.size.chip }

        updateMoveX chip =
            let
                ( x, _ ) =
                    chip.point

                ( xo, _ ) =
                    chip.old_point
            in
            if x /= xo then
                { chip | x_a = animateMoveChip model x xo }

            else
                { chip | x_a = A.static <| model.size.box * toFloat x }

        updateMoveY chip =
            let
                ( _, y ) =
                    chip.point

                ( _, yo ) =
                    chip.old_point
            in
            if y /= yo then
                { chip | y_a = animateMoveChip model y yo }

            else
                { chip | y_a = A.static <| model.size.box * toFloat y }

        ( update_id, update_chips ) =
            update_ model.n next []

        getValueCreate chips =
            List.foldl
                (\a b ->
                    if a.new then
                        a.value + b

                    else
                        b
                )
                0
                chips

        new_model =
            { model
                | chips = update_chips
                , n = update_id
                , score = model.score + getValueCreate next
            }

        haveMove =
            List.foldl (\a b -> b || not (G.equalPoint a.point a.old_point))
                False
                next
    in
    if haveMove then
        ( new_model, randomChipCmd new_model )

    else
        ( model, Cmd.none )


{-| Create two random number: Point and Value for new chip
and create Msg for create new chip in game
-}
randomChipCmd : Model -> Cmd Msg
randomChipCmd model =
    let
        empty =
            G.getListEmptyPoint classChipInstance model.chips

        new_chip : Random.Generator ( Int, Int )
        new_chip =
            Random.pair (Random.int 0 (List.length empty - 1)) (Random.int 0 9)
    in
    Random.generate (NewChip empty) new_chip


resize : Model -> Int -> Int -> Model
resize model width height =
    let
        app =
            toFloat <|
                if width > height then
                    height

                else
                    width

        game =
            app * 80 / 100

        padding_game =
            app * 9 / 100

        box =
            game * 25 / 100

        padding_chip =
            game * 25 / 100 * 5 / 100

        chip =
            box - (padding_chip * 2)

        text =
            app * 2.5 / 100

        padding_text =
            app * 92.2 / 100

        score =
            app * 2.5 / 100

        padding_score =
            app * 1 / 100

        size =
            Size app
                (game - 2)
                (padding_game - 1)
                box
                padding_chip
                chip
                text
                padding_text
                score
                padding_score
    in
    { model
        | width = toFloat width
        , height = toFloat height
        , size = size
        , chips = stopAnimation size model.chips
    }


{-| Create new chip in game
-}
newChip : Model -> List G.Point -> Int -> Int -> ( Model, Cmd Msg )
newChip model list index value =
    let
        create =
            G.createChip
                classChipInstance
                defaultChip
                (MList.byIndexFromList G.defaultPoint list index)
                value

        new =
            let
                ( x, y ) =
                    create.point
            in
            { model
                | need_chip =
                    if model.need_chip /= 0 then
                        model.need_chip - 1

                    else
                        0
                , chips =
                    { create
                        | id = model.n
                        , x_a = A.static <| model.size.box * toFloat x
                        , y_a = A.static <| model.size.box * toFloat y
                        , size_a = animateCreateChip model
                    }
                        :: model.chips
                , n = model.n + 1
            }
    in
    ( new
    , if model.need_chip > 1 then
        randomChipCmd new

      else
        Cmd.none
    )


{-| time create and delete chip in millisecond
-}
timeCreateDelete =
    200


{-| time move chip in millisecond
-}
timeMove =
    200


stopAnimation : Size -> List Chip -> List Chip
stopAnimation size list =
    let
        stopAnimation_ i =
            let
                ( x, y ) =
                    i.point
            in
            { i
                | size_a = A.static size.chip
                , x_a = A.static (size.box * toFloat x)
                , y_a = A.static (size.box * toFloat y)
            }
    in
    List.map stopAnimation_ list


animateCreateChip : Model -> A.Animation
animateCreateChip model =
    A.animation model.clock
        |> A.from 0
        |> A.to model.size.chip
        |> A.duration timeCreateDelete
        |> A.delay timeMove


animateDeleteChip : Model -> A.Animation
animateDeleteChip model =
    A.animation model.clock
        |> A.from model.size.chip
        |> A.to 0
        |> A.duration timeCreateDelete
        |> A.delay timeMove


animateMoveChip : Model -> Int -> Int -> A.Animation
animateMoveChip model new old =
    A.animation model.clock
        |> A.from (model.size.box * toFloat old)
        |> A.to (model.size.box * toFloat new)
        |> A.duration timeMove
